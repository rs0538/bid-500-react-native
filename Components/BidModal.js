import React, { Component } from 'react';
import { Modal, Text, TouchableHighlight, View, Alert, Button, Picker } from 'react-native';

export class BidModal extends Component {
    constructor(props) {
        super();
        this.state = { modalVisible: false, bidNumber: '', bidSuit: '' };

    }

    resetState() {
        this.setState({ modalVisible: false, bidNumber: '', bidSuit: '' });
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    render() {
        return (
            <View style={{ marginTop: 22 }}>
                <Button title="Bid" onPress={() => Alert.alert(
                    'Alert Title',
                    'My Alert Msg',
                    [
                        { text: 'Yes', onPress: () => this.setModalVisible(true) },
                        { text: 'Pass', style: 'cancel' }
                    ],
                    { cancelable: false }
                )} />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={{ width: "100%", flexDirection: "row" }} >
                        <Text style={{ width: "50%" }}>
                            Select number of bids:
                            </Text>
                        <Picker
                            selectedValue={this.state.bidNumber}
                            style={{ height: 50, width: "50%" }}
                            onValueChange={(itemValue) => this.setState({ bidNumber: itemValue })}>
                            <Picker.Item label="6" value="6"></Picker.Item>
                            <Picker.Item label="7" value="7"></Picker.Item>
                            <Picker.Item label="8" value="8"></Picker.Item>
                            <Picker.Item label="9" value="9"></Picker.Item>
                            <Picker.Item label="10" value="10"></Picker.Item>
                        </Picker>
                    </View>
                    <View style={{ width: "100%", flexDirection: "row" }} >
                        <Text style={{ width: "50%" }}>
                            Select suit:
                            </Text>
                        <Picker
                            selectedValue={this.state.bidSuit}
                            style={{ height: 50, width: "50%" }}
                            onValueChange={(itemValue) => this.setState({ bidSuit: itemValue })}>
                            <Picker.Item label="No Trump" value="noTrump"></Picker.Item>
                            <Picker.Item label="Hearts" value="heart"></Picker.Item>
                            <Picker.Item label="Diamonds" value="diamond"></Picker.Item>
                            <Picker.Item label="Clubs" value="club"></Picker.Item>
                            <Picker.Item label="Spades" value="spade"></Picker.Item>
                        </Picker>
                    </View>
                    <View style={{ flexDirection: "row", width: "100%" }}>
                        <Button title="Bid!" color={"green"} style={{ right: 0 }}
                            onPress={() => {
                                this.setModalVisible(!this.state.modalVisible);
                                this.resetState();
                            }}>
                            <Text>Hide Modal</Text>
                        </Button>
                        <Button title="Close" style={{left:0}}
                            onPress={() => {
                                this.setModalVisible(!this.state.modalVisible);
                                this.resetState();
                            }}>
                            <Text>Hide Modal</Text>
                        </Button>
                    </View>
                </Modal>
            </View>
        );
    }
}