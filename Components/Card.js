import React, { Component } from 'react';
import { StyleSheet, Image } from 'react-native';
import { View, Text } from 'react-native';
import ImageConstants from '../images/ImageConstants'
import CardConstants from '../CardConstants';
export default class Card extends Component {
    constructor(props) {
        super();
        this.faceValue = props.faceValue ? props.faceValue : "K";
        this.suit = props.suit ? props.suit : CardConstants.Club;
        this.imageName = ImageConstants.SuitToSuitImage[this.suit];

    }

    render() {
        return (
            <View style={CardStyle.Card}>
                <Text>{this.faceValue}</Text>
                <Image source={ImageConstants.SuitToSuitImage[this.suit]} />
            </View>
        )
    }
}
const CardStyle = StyleSheet.create({
    Card: {
        borderWidth: 4,
        borderColor: "black",
        backgroundColor: "white",
        width: "20%",
        height: "45%",
        paddingTop: 1
    }
});