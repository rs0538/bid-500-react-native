import React, { Component } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { View } from 'react-native';
import CardConstants from '../CardConstants';
import Card from './Card';

export default class CardHolder extends Component {
    constructor(props) {
        super();
        this.hand = props.hand;
    }

    render() {
        return (
            <View style={CardHolderStyle.CardHolder}>
                {
                    this.hand.map(x => x.card)
                }
            </View>
        );
    }

    GenerateDeck() {
        var cards = [];

        for (let i = 0; i < CardConstants.Suits.length; i++) {
            for (let j = 0; j < CardConstants.FaceValues.length; j++) {
                let summedId = i + j;
                let suit = CardConstants.Suits[i];
                let cardValue = CardConstants.FaceValues[j];
                cards.push({ key: summedId, suit: suit, value: cardValue });
            }
        }
    }

}
const CardHolderStyle = StyleSheet.create({
    CardHolder: {
        width: '100%',
        height: '30%',
        borderColor: 'white',
        borderWidth: 1,
        position: "absolute",
        bottom: 0,
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap"
    }
});