import * as React from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert } from 'react-native';
import CardHolder from './Components/CardHolder';
import Card from './Components/Card';
import CardConstants from './CardConstants'
import { BidModal } from './Components/BidModal';
const instructions = Platform.select({
  ios: `Press Cmd+R to reload,\nCmd+D or shake for dev menu`,
  android: `Double tap R on your keyboard to reload,\nShake or press menu button for dev menu`,
});

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={{ color: 'white' }}>really did change</Text>
      <BidModal />
      <CardHolder hand={[{ "card": <Card Key={1} suit={CardConstants.Heart} faceValue={8} /> }, { "card": <Card Key={2} /> }, { "card": <Card Key={3} /> }, { "card": <Card Key={4} /> }, { "card": <Card Key={5} /> }, { "card": <Card Key={6} /> }, { "card": <Card Key={7} /> }, { "card": <Card Key={8} /> }, { "card": <Card Key={9} /> }, { "card": <Card Key={10} /> }]} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
