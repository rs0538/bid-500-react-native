export default {
    heartImage: "../images/heartIcon.png",
    diamondImage: "../images/diamondIcon.png",
    clubImage: "../images/clubIcon.png",
    spadeImge: "../images/spadeIcon.png",

    SuitToSuitImage: {
        "heart": require("../images/heartIcon.png"),
        "diamond": require("../images/diamondIcon.png"),
        "club": require("../images/clubIcon.png"),
        "spade": require("../images/spadeIcon.png")
    }
}