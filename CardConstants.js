export default {
    Heart: 'heart',
    Diamond: 'diamond',
    Club: 'club',
    Spade: 'spade',
    Suits: ["heart", "diamond", "club", "space"],
    FaceValues: ["4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "Joker"],
    CardsToFaceValueLookup: [
        { "4": 4 },
        { "5": 5 },
        { "6": 6 },
        { "7": 7 },
        { "8": 8 },
        { "9": 9 },
        { "10": 10 },
        { "OffSuitJack": 11 },
        { "Queen": 12 },
        { "King": 13 },
        { "Ace": 14 },
        { "Jick": 15 },
        { "Jack": 16 },
        { "Joker": 17 }
    ]
};